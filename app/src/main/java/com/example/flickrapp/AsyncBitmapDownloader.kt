package com.example.flickrapp

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.wifi.WifiConfiguration.AuthAlgorithm.strings
import android.os.AsyncTask
import android.util.Log
import android.widget.ImageView
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class AsyncBitmapDownloader(image: ImageView) : AsyncTask<String?, Void?, Bitmap?>() {
    private val imageViewReference: WeakReference<ImageView>
    override fun doInBackground(vararg params: String?): Bitmap? {
        var connection: HttpURLConnection? = null
        try {
            val url = URL(params[0])
            connection = url.openConnection() as HttpURLConnection
            val instream: InputStream = BufferedInputStream(connection.inputStream)
            return BitmapFactory.decodeStream(instream)
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            connection!!.disconnect()
        }
        return null
    }

    override fun onPostExecute(bitmap: Bitmap?) {
        if (bitmap == null) {
            Log.i("JFL", "Error downloading image.")
            return
        }
        val imageView = imageViewReference.get()
        imageView?.setImageBitmap(bitmap)
    }

    init {
        imageViewReference = WeakReference(image)
    }

}