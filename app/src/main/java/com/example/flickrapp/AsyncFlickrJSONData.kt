package com.example.flickrapp

import android.os.AsyncTask
import android.util.Log
import android.widget.ImageView
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class AsyncFlickrJSONData(image: ImageView) : AsyncTask<String?, Void?, JSONObject?>() {
    private val imageViewReference: WeakReference<ImageView>
    override fun doInBackground(vararg params: String?): JSONObject? {
        var connection: HttpURLConnection? = null
        try {
            val url = URL(params[0])
            connection = url.openConnection() as HttpURLConnection
            val instream: InputStream = BufferedInputStream(connection.inputStream)
            val s = readStream(instream)
            return JSONObject(s.substring(15, s.length - 1))
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: JSONException) {
            e.printStackTrace()
        } finally {
            connection!!.disconnect()
        }
        return null
    }

    override fun onPostExecute(jsonObject: JSONObject?) {
        if (jsonObject == null) {
            Log.i("JFL", "Failure")
            return
        }
        Log.i("JFL", jsonObject.toString())
        try {
            val items = jsonObject.getJSONArray("items")
            val first = items[0] as JSONObject
            val media = first.getJSONObject("media")
            val link = media.getString("m")
            val downloader = AsyncBitmapDownloader(imageViewReference.get()!!)
            downloader.execute(link)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    @Throws(IOException::class)
    private fun readStream(instream: InputStream): String {
        val sb = StringBuilder()
        val r = BufferedReader(InputStreamReader(instream))
        var line = r.readLine()
        while (line != null) {
            sb.append(line)
            line = r.readLine()
        }
        instream.close()
        return sb.toString()
    }

    init {
        imageViewReference = WeakReference(image)
    }
}