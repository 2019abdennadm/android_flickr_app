package com.example.flickrapp

import android.os.Bundle
import android.view.View
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity

class ListActivity : AppCompatActivity() {
    var list: ListView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        list = findViewById<View>(R.id.list) as ListView
        val adapter = MyAdapter(list!!.context)
        list!!.adapter = adapter
        val linkCollector = AsyncFlickrJSONDataList(adapter)
        linkCollector.execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json")
    }
}