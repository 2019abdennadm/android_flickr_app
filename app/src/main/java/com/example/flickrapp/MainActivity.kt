package com.example.flickrapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    var getImage: Button? = null
    var image: ImageView? = null
    var getLinks: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getImage = findViewById<View>(R.id.get_image) as Button
        getLinks = findViewById<View>(R.id.get_links) as Button
        image = findViewById<View>(R.id.image) as ImageView
        getImage!!.setOnClickListener { view: View? ->
            val task = AsyncFlickrJSONData(image!!)
            task.execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json")
        }
        getLinks!!.setOnClickListener { view: View? ->
            val intent = Intent(applicationContext, ListActivity::class.java)
            startActivity(intent)
        }
    }
}