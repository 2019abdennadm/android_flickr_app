package com.example.flickrapp

import android.content.Context
import android.graphics.Bitmap
import android.os.AsyncTask
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.Response
import com.android.volley.toolbox.ImageRequest
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*

internal class AsyncFlickrJSONDataList(var adapter: MyAdapter) : AsyncTask<String?, Void?, JSONObject?>() {
    override fun doInBackground(vararg params: String?): JSONObject? {
        var connection: HttpURLConnection? = null
        try {
            val url = URL(params[0])
            params[0]?.let { Log.i("JFL", it) }
            connection = url.openConnection() as HttpURLConnection
            val instream: InputStream = BufferedInputStream(connection.inputStream)
            val s = readStream(instream)
            Log.i("JFL", "Response String $s")
            return JSONObject(s.substring(15, s.length - 1))
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: JSONException) {
            e.printStackTrace()
        } finally {
            connection!!.disconnect()
        }
        return null
    }

    override fun onPostExecute(jsonObject: JSONObject?) {
        if (jsonObject == null) return
        try {
            val list = jsonObject.getJSONArray("items")
            var i = 0
            val l = list.length()
            while (i < l) {
                val link = list.getJSONObject(i).getJSONObject("media").getString("m")
                adapter.dd(link)
                Log.i("JFL", "Adding to adapter url : $link")
                i++
            }
            adapter.notifyDataSetChanged()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    @Throws(IOException::class)
    private fun readStream(instream: InputStream): String {
        val sb = StringBuilder()
        val r = BufferedReader(InputStreamReader(instream))
        var line = r.readLine()
        while (line != null) {
            sb.append(line)
            line = r.readLine()
        }
        instream.close()
        return sb.toString()
    }

}

class MyAdapter internal constructor(private val context: Context) : BaseAdapter() {
    private val links: Vector<String>
    override fun getCount(): Int {
        return links.size
    }

    override fun getItem(position: Int): Any {
        return links[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun dd(link: String) {
        links.add(link)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val link = getItem(position) as String
        var cv: View;
        if ( convertView == null )
            cv = LayoutInflater.from(context).inflate(R.layout.bitmap_list, parent, false)
        else cv = convertView;

        val linkBox = cv.findViewById<View>(R.id.list_bitmap_link) as TextView
        val linkView = cv.findViewById<View>(R.id.list_bitmap) as ImageView
        linkBox.text = link
        val r = ImageRequest(
                link,
                Response.Listener { response -> // Do something with response
                    linkView.setImageBitmap(response)
                },
                0,
                0,
                ImageView.ScaleType.CENTER_CROP,
                Bitmap.Config.RGB_565,
                Response.ErrorListener { error ->
                    // Error listener
                    // Do something with error response
                    error.printStackTrace()
                }
        )
        MySingleton.getInstance(context)?.addToRequestQueue(r)
        return cv
    }

    init {
        links = Vector()
    }
}