package com.example.flickrapp

import android.content.Context
import android.graphics.Bitmap
import android.util.LruCache
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.Volley

class MySingleton private constructor(context: Context) {
    private var rq: RequestQueue?
    val imageLoader: ImageLoader

    // getApplicationContext() is key, it keeps you from leaking the
    // Activity or BroadcastReceiver if someone passes one in.
    val requestQueue: RequestQueue?
        get() {
            if (rq == null) {
                // getApplicationContext() is key, it keeps you from leaking the
                // Activity or BroadcastReceiver if someone passes one in.
                rq = Volley.newRequestQueue(ctx.applicationContext)
            }
            return rq
        }

    fun <T> addToRequestQueue(req: Request<T>?) {
        requestQueue!!.add(req)
    }

    companion object {
        private var instance: MySingleton? = null
        private lateinit var ctx: Context

        @Synchronized
        fun getInstance(context: Context): MySingleton? {
            if (instance == null) {
                instance = MySingleton(context)
            }
            return instance
        }
    }

    init {
        ctx = context
        rq = requestQueue
        imageLoader = ImageLoader(rq, object : ImageLoader.ImageCache {
            private val cache = LruCache<String, Bitmap>(20)
            override fun getBitmap(link: String): Bitmap {
                return cache[link]
            }

            override fun putBitmap(link: String, bitmap: Bitmap) {
                cache.put(link, bitmap)
            }
        })
    }
}